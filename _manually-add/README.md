# Manual installation

Please verify each line before you blindly run them, as they may not suit your
particular use case.

```sh
# Disable mouse acceleration (make sure that you want this per device)
# Only necessary when not configurable in your GUI (e.g. using i3)
cp 50-mouse-acceleration.conf /etc/X11/xorg.conf.d/50-mouse-acceleration.conf
```
