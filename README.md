# Dano's dot files

Dotfile repository using GNU Stow. Based on:
<http://brandon.invergo.net/news/2012-05-26-using-gnu-stow-to-manage-your-dotfiles.html>

Submodules:
zsh/.oh-my-zsh/

# Installation

* Install GNU Stow.
* Run Stow on each subdirectory whose dotfiles you want to install.
* Install dependencies

## Dependencies

Some configs (such as i3) assume that certain programs are installed.

### ranger

* trash-cli: to map DD to putting file in trash

### Caveats

Certain directories should be created before running Stow on your dotfile
subdirectories. This is because Stow does not know whether subdirectories should
be symlinked or created "normally", and will therefore create a symlink from the
first subdirectory that does not exist, e.g. `~/.config/ranger` should be
created first, otherwise `bookmarks`, `tagged`, etc. will be created inside
this dotfiles directory.

Files in `_manually-add` need to be manually installed, rather than handled
by Stow.
